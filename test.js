console.log("Initializing Kengi");
let game = require('./build/Release/kengi.node')();

let mass = 10;
let size = 0.5;
let allBits = 4294967295;//32-bit mask
let cpvzero = {x:0, y:0};
let pos = {x:0, y:0};
let moment = game.cpMomentForCircle(mass, 0, size, cpvzero);
let staticBodySize = 1;

console.log("Initializing cpSpace");
let space = game.cpSpaceNew();
game.cpSpaceSetDamping(space, 0.002);
game.cpSpaceUseSpatialHash(space, staticBodySize, 1000);

console.log("Initializing cpBody");
let body = game.cpBodyNew(mass, moment);
game.cpSpaceAddBody(space, body);
game.cpBodySetPosition(body, pos);
console.log(body.getHashCode());

console.log("Initializing cpShape");
let shape = game.cpCircleShapeNew(body, size, cpvzero);
game.cpShapeSetElasticity(shape, 0.8);
game.cpShapeSetCollisionType(shape, 1);
console.log(game.cpShapeGetCollisionType(shape));
game.cpShapeSetFilter(shape, {group:0, categories:allBits, mask:allBits});
console.log(JSON.stringify(game.cpShapeGetFilter(shape)));
game.cpSpaceAddShape(space, shape);
console.log(shape.getHashCode());

console.log("Initializing static cpBody and cpShape");
let staticBody = game.cpBodyNewStatic();
game.cpSpaceAddBody(space, staticBody);
game.cpBodySetPosition(staticBody, {x: 2, y: 0});
let polygon = [
    {"x":staticBodySize, "y":staticBodySize},
    {"x":staticBodySize, "y":-staticBodySize},
    {"x":-staticBodySize,  "y":-staticBodySize},
    {"x":-staticBodySize,  "y":staticBodySize}
];
let transform = {a:1,b:0,c:0,d:1,tx:0,ty:0};
let polyShape = game.cpPolyShapeNew(staticBody, polygon.length, polygon, transform, 0);
game.cpShapeSetCollisionType(polyShape, 2);
console.log(game.cpShapeGetCollisionType(polyShape));
game.cpShapeSetFilter(polyShape, {group:0, categories:allBits, mask:allBits});
console.log(JSON.stringify(game.cpShapeGetFilter(polyShape)));
game.cpSpaceAddShape(space, polyShape);

console.log("Initializing collision callback");
function onCollision(a, b){
    setTimeout(()=>{
        console.log("onCollision");
        let aBody = game.cpShapeGetBody(a);
        let bBody = game.cpShapeGetBody(b);
        console.log(aBody.getHashCode());
        console.log(a.getHashCode());
        console.log(JSON.stringify(game.cpBodyGetPosition(aBody)) + JSON.stringify(game.cpBodyGetVelocity(aBody)));
        console.log(JSON.stringify(game.cpBodyGetPosition(bBody)) + JSON.stringify(game.cpBodyGetVelocity(bBody)));        
    });
    return true;
}
game.cpSpaceAddCollisionHandler(space, 1, 2, onCollision);

console.log("Simulating 10 frames of movement");
for(let i = 0; i !== 10; i++){
    if(i > 0 && i < 6){
        game.cpBodyApplyImpulseAtLocalPoint(body, {x:25.0, y:0.0}, {x:0, y:0});
    }
    game.cpSpaceStep(space, 1/10);
    console.log(JSON.stringify(game.cpBodyGetPosition(body)) + JSON.stringify(game.cpBodyGetVelocity(body)));    
}

console.log("Initializing cpBody");
let body2 = game.cpBodyNew(mass, moment);
game.cpSpaceAddBody(space, body2);
game.cpBodySetPosition(body2, {x: 4.2, y: 8.4});

console.log("Initializing cpShape");
let shape2 = game.cpCircleShapeNew(body2, size, cpvzero);
game.cpShapeSetElasticity(shape2, 0.8);
game.cpShapeSetFilter(shape2, {group:0, categories:allBits, mask:allBits});
game.cpSpaceAddShape(space, shape2);

console.log("Querying for shapes around point");
let queryResult = game.cpSpacePointQuery(space, {x:0,y:0}, 100, {group:0, categories:allBits, mask:allBits});
for(let i = 0; i !== queryResult.length; i++){
    let shapeQueried = queryResult[i];
    let bodyQueried = game.cpShapeGetBody(shapeQueried);
    console.log(JSON.stringify(game.cpBodyGetPosition(bodyQueried)) + JSON.stringify(game.cpBodyGetVelocity(bodyQueried)));    
}

console.log("Querying for shapes along line");
queryResult = game.cpSpaceSegmentQuery(space, {x:0,y:0}, {x:5,y:9}, 2, {group:0, categories:allBits, mask:allBits});
for(let i = 0; i !== queryResult.length; i++){
    let shapeQueried = queryResult[i];
    let bodyQueried = game.cpShapeGetBody(shapeQueried);
    console.log(JSON.stringify(game.cpBodyGetPosition(bodyQueried)) + JSON.stringify(game.cpBodyGetVelocity(bodyQueried)));    
}

setTimeout(()=>{
    console.log("Removing cpShape");
    game.cpSpaceRemoveShape(space, shape);
    game.cpShapeFree(shape);
    
    console.log("Removing cpShape");
    game.cpSpaceRemoveShape(space, shape2);
    game.cpShapeFree(shape2);
    
    console.log("Removing cpBody");
    game.cpSpaceRemoveBody(space, body);
    game.cpBodyFree(body);
    
    console.log("Removing cpBody");
    game.cpSpaceRemoveBody(space, body2);
    game.cpBodyFree(body2);
    
    console.log("Removing static cpBody and cpShape");
    game.cpSpaceRemoveShape(space, polyShape);
    game.cpSpaceRemoveBody(space, staticBody);
    game.cpShapeFree(polyShape);
    game.cpBodyFree(staticBody);
    
    console.log("Clearing collision handler");
    game.cpSpaceAddCollisionHandler(space, 1, 2, null);
    
    console.log("Removing cpSpace");
    game.cpSpaceFree(space);

    console.log("Creating pathfindGrid");
    let pathfindGrid = game.createPathfindGrid();
    console.log("Resizing pathfindGrid");
    game.resizePathfindGrid(pathfindGrid, 8, 12);
    console.log("Setting node weight");
    game.setPathfindNodeWeight(pathfindGrid, {x: 4, y:4}, allBits);
    game.setPathfindNodeWeight(pathfindGrid, {x: 4, y:5}, allBits);
    game.setPathfindNodeWeight(pathfindGrid, {x: 4, y:6}, allBits);
    game.setPathfindNodeWeight(pathfindGrid, {x: 4, y:7}, allBits);
    game.setPathfindNodeWeight(pathfindGrid, {x: 4, y:8}, allBits);
    console.log("Pathfinding");
    let path = game.pathfind(pathfindGrid, {x:0, y:0}, {x:7, y:11}, 10, 1000);
    for(let y = 0; y !== 12; y++){
        let row = '';
        for(let x = 0; x !== 8; x++){
            let pt = path.find((v)=>{return v.x==x && v.y==y;})
            if(pt){
                row += '1';
            } else {
                row += '0';
            }
        }
        console.log(row);
    }
    console.log("Raycasting");
    console.log(game.raycast(pathfindGrid, {x:0, y:2}, {x:7, y:2}));
    console.log(game.raycast(pathfindGrid, {x:0, y:6}, {x:7, y:6}));
    console.log(game.raycast(pathfindGrid, {x:0, y:7}, {x:7, y:7}));
    console.log(game.raycast(pathfindGrid, {x:0, y:9}, {x:7, y:9}));

    console.log("Done");    
});
