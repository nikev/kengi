#ifndef __CPSHAPEREF_H__
#define __CPSHAPEREF_H__

#include "nan.h"
#include <memory>
#include "chipmunk/chipmunk.h"

class cpShapeRef : public Nan::ObjectWrap {
  cpShapeRef();
  virtual ~cpShapeRef();

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static Nan::Persistent<v8::Function> constructor;
  static Nan::Persistent<v8::FunctionTemplate> constructorTemplate;

 public:
  static void GetHashCode(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void Init();
  static v8::Local<v8::Object> NewInstance();
  static bool HasInstance(const v8::Local<v8::Object>& obj);
  std::weak_ptr<cpShape> shape;
};

#endif
