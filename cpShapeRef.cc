#include "cpShapeRef.h"

using namespace v8;

Nan::Persistent<Function> cpShapeRef::constructor;
Nan::Persistent<FunctionTemplate> cpShapeRef::constructorTemplate;

cpShapeRef::cpShapeRef() {
}

cpShapeRef::~cpShapeRef() {
}

void cpShapeRef::Init() {
  Nan::HandleScope scope;
  Local<FunctionTemplate> newTemplate = Nan::New<FunctionTemplate>(New);
  newTemplate->SetClassName((Nan::New("cpShapeRef").ToLocalChecked()));
  newTemplate->InstanceTemplate()->SetInternalFieldCount(1);
  newTemplate->PrototypeTemplate()->Set(Nan::New("getHashCode").ToLocalChecked(),Nan::New<FunctionTemplate>(GetHashCode));
  constructor.Reset(newTemplate->GetFunction());
  constructorTemplate.Reset(newTemplate);
}

void cpShapeRef::GetHashCode(const Nan::FunctionCallbackInfo<v8::Value>& args){
  Local<Object> instance = args.This();
  cpShapeRef* shapeRef = ObjectWrap::Unwrap<cpShapeRef>(instance);
  std::shared_ptr<cpShape> sharedShape = shapeRef->shape.lock();
  if(sharedShape){
    args.GetReturnValue().Set(Nan::New((unsigned)(size_t)sharedShape.get()));
  }
}

void cpShapeRef::New(const Nan::FunctionCallbackInfo<Value>& args) {
    cpShapeRef* obj = new cpShapeRef();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
}

Local<Object> cpShapeRef::NewInstance() {
  Nan::EscapableHandleScope scope;
  Local<Function> cons = Nan::New<Function>(constructor);
  Local<Object> instance = cons->NewInstance();
  return scope.Escape(instance);
}

bool cpShapeRef::HasInstance(const Local<Object>& obj){
  return Nan::New(constructorTemplate)->HasInstance(obj);
}
