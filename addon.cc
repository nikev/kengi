#include <nan.h>
#include "kengi.h"
#include "cpSpaceRef.h"
#include "cpBodyRef.h"
#include "cpShapeRef.h"
#include "pathfindGridRef.h"

using namespace v8;

void CreateObject(const Nan::FunctionCallbackInfo<v8::Value>& info) {
  info.GetReturnValue().Set(Kengi::NewInstance());
}

void InitAll(Handle<Object> exports, Handle<Object> module) {
  Nan::HandleScope scope;

  Kengi::Init();
  cpSpaceRef::Init();
  cpBodyRef::Init();
  cpShapeRef::Init();
  pathfindGridRef::Init();

  module->Set(Nan::New("exports").ToLocalChecked(),
      Nan::New<v8::FunctionTemplate>(CreateObject)->GetFunction());
}

NODE_MODULE(addon, InitAll)
