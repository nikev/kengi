#ifndef KENGI_H
#define KENGI_H

#include "nan.h"
#include <memory>
#include "chipmunk/chipmunk.h"

class Kengi : public Nan::ObjectWrap {
  Kengi();
  virtual ~Kengi();

  static void null_deleter(void*) {}
  static void New(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static Nan::Persistent<v8::Function> constructor;

	static void queryFunc(cpShape *shape, cpVect point, cpFloat distance, cpVect gradient, void *data);
  static void segmentQueryFunc(cpShape *shape, cpVect point, cpVect normal, cpFloat alpha, void *data);
  static unsigned char collisionHandler(cpArbiter *arb, struct cpSpace *space, cpDataPointer data);
  static void queryBody(cpBody* body, void* data);
  static void queryShape(cpShape* shape, void* data);
	static std::vector<cpShape*> queryList;
  static unsigned queryBodyCount;
  static unsigned queryShapeCount;

  static void cpSpaceNew(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceFree(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceSetGravity(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceSetDamping(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceUseSpatialHash(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceStep(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceAddBody(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceAddShape(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceRemoveBody(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceRemoveShape(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpacePointQuery(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceSegmentQuery(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpSpaceAddCollisionHandler(const Nan::FunctionCallbackInfo<v8::Value>& args);

  static void cpBodyNew(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyNewStatic(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyFree(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyGetVelocity(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodySetVelocity(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyGetPosition(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodySetPosition(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodySetForce(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyGetForce(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyApplyForceAtLocalPoint(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyApplyImpulseAtLocalPoint(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyGetMass(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodySetMass(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpBodyGetRotation(const Nan::FunctionCallbackInfo<v8::Value>& args);

  static void cpShapeFree(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeSetFriction(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeSetElasticity(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeGetBody(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeSetCollisionType(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeGetCollisionType(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeSetFilter(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpShapeGetFilter(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpCircleShapeNew(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpPolyShapeNew(const Nan::FunctionCallbackInfo<v8::Value>& args);

  static void cpMomentForCircle(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void cpMomentForBox(const Nan::FunctionCallbackInfo<v8::Value>& args);

  static void createPathfindGrid(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void resizePathfindGrid(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void setPathfindNodeWeight(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void pathfind(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static void raycast(const Nan::FunctionCallbackInfo<v8::Value>& args);

 public:
  static void Init();
  static v8::Local<v8::Object> NewInstance();
  static Nan::Persistent<v8::FunctionTemplate> constructorTemplate;
};

#endif
