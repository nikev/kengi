#include "pathfindGridRef.h"

using namespace v8;

Nan::Persistent<Function> pathfindGridRef::constructor;
Nan::Persistent<FunctionTemplate> pathfindGridRef::constructorTemplate;

pathfindGridRef::pathfindGridRef()
{
}

pathfindGridRef::~pathfindGridRef()
{
}

void pathfindGridRef::Init()
{
  Nan::HandleScope scope;
  Local<FunctionTemplate> newTemplate = Nan::New<FunctionTemplate>(New);
  newTemplate->SetClassName((Nan::New("pathfindGridRef").ToLocalChecked()));
  newTemplate->InstanceTemplate()->SetInternalFieldCount(1);
  constructor.Reset(newTemplate->GetFunction());
  constructorTemplate.Reset(newTemplate);
}

void pathfindGridRef::New(const Nan::FunctionCallbackInfo<Value> &args)
{
  pathfindGridRef *obj = new pathfindGridRef();
  obj->Wrap(args.This());
  args.GetReturnValue().Set(args.This());
}

Local<Object> pathfindGridRef::NewInstance()
{
  Nan::EscapableHandleScope scope;
  Local<Function> cons = Nan::New<Function>(constructor);
  Local<Object> instance = cons->NewInstance();
  return scope.Escape(instance);
}

bool pathfindGridRef::HasInstance(const Local<Object> &obj)
{
  return Nan::New(constructorTemplate)->HasInstance(obj);
}
