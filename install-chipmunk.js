const { exec } = require('child_process');
const fs = require('fs');


if(!fs.existsSync(__dirname + '/Chipmunk2D')){
    console.log('Cloning Chipmunk2D');
    exec('git clone https://github.com/slembcke/Chipmunk2D', (err, stdout, stderr) => {
        if (err) {
            console.log('Error running "git clone https://github.com/slembcke/Chipmunk2D"');
            return;
        }
        exec('git checkout Chipmunk-7.0.2', {cwd:'Chipmunk2D'}, (err, stdout, stderr) => {
            if (err) {
                console.log('Error running "git checkout Chipmunk-7.0.2"');
                return;
            }
            //replacing "{}" with "{0}" in cpPolyline.c to fix an error on some compilers
            let filename = __dirname + '/Chipmunk2D/src/cpPolyline.c';
            fs.readFile(filename, 'utf8', function (err, data) {
                if (err) return console.log(err);
                var result = data.replace(/{}/g, '{0}');
                fs.writeFile(filename, result, 'utf8', function (err) {
                    if (err) return console.log(err);
                });
            });
        });
    }); 
}
