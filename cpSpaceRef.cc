#include "cpSpaceRef.h"

using namespace v8;

Nan::Persistent<Function> cpSpaceRef::constructor;
Nan::Persistent<FunctionTemplate> cpSpaceRef::constructorTemplate;

cpSpaceRef::cpSpaceRef(): handlerCount(0) {
}

cpSpaceRef::~cpSpaceRef() {
}

void cpSpaceRef::Init() {
  Nan::HandleScope scope;
  Local<FunctionTemplate> newTemplate = Nan::New<FunctionTemplate>(New);
  newTemplate->SetClassName((Nan::New("cpSpaceRef").ToLocalChecked()));
  newTemplate->InstanceTemplate()->SetInternalFieldCount(1);
  constructor.Reset(newTemplate->GetFunction());
  constructorTemplate.Reset(newTemplate);
}

void cpSpaceRef::New(const Nan::FunctionCallbackInfo<Value>& args) {
    cpSpaceRef* obj = new cpSpaceRef();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
}

Local<Object> cpSpaceRef::NewInstance() {
  Nan::EscapableHandleScope scope;
  Local<Function> cons = Nan::New<Function>(constructor);
  Local<Object> instance = cons->NewInstance();
  return scope.Escape(instance);
}

bool cpSpaceRef::HasInstance(const Local<Object>& obj){
  return Nan::New(constructorTemplate)->HasInstance(obj);
}
