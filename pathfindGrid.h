#ifndef __PATHFIND_GRID_H__
#define __PATHFIND_GRID_H__

#include "nan.h"
#include <memory>
#include <vector>
#include <iostream>

class vec2i {
  public:
  int x;
  int y;
  vec2i(): x(0), y(0){}
  vec2i(int _x, int _y): x(_x), y(_y){}
};

class Node {
    public:
    bool open;
    Node* parent;
    vec2i pos;
    unsigned fcost;
    unsigned weight;
    Node(): open(true), parent(0), fcost(0), weight(0) {}
};

class pathfindGrid {
  public:
  std::vector<std::vector<Node> > nodes;
  std::vector<char> openSetLookup;
  std::vector<char> closeSet;
  std::vector<Node*> openSet;
  unsigned openSetCount;

  pathfindGrid(): openSetCount(0){}
  
  void resize(unsigned width, unsigned height);
  inline unsigned getNodeId(const vec2i& pos);
  inline bool outofbounds(const vec2i& pos);
  void setNodeWeight(const vec2i& pos, unsigned weight);
  std::vector<vec2i> pathfind(const vec2i& a, const vec2i& b, int hStrength, int cycleLimit);
  bool raycast(const vec2i& a, const vec2i& b);
};

#endif
