#include "kengi.h"
#include "cpSpaceRef.h"
#include "cpBodyRef.h"
#include "cpShapeRef.h"
#include "pathfindGrid.h"
#include "pathfindGridRef.h"
#include "chipmunk/chipmunk_structs.h"
#include <iostream>

using namespace v8;

Nan::Persistent<Function> Kengi::constructor;
Nan::Persistent<FunctionTemplate> Kengi::constructorTemplate;
std::vector<cpShape*> Kengi::queryList;
unsigned Kengi::queryBodyCount = 0;
unsigned Kengi::queryShapeCount = 0;

Kengi::Kengi() {
}

Kengi::~Kengi() {
}

void Kengi::Init() {
  Nan::HandleScope scope;
  Local<FunctionTemplate> functionTemplate = Nan::New<FunctionTemplate>(New);
  functionTemplate->SetClassName((Nan::New("Kengi").ToLocalChecked()));
  functionTemplate->InstanceTemplate()->SetInternalFieldCount(5);
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceNew").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceNew));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceFree").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceFree));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceSetGravity").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceSetGravity));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceSetDamping").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceSetDamping));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceUseSpatialHash").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceUseSpatialHash));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceStep").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceStep));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceAddBody").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceAddBody));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceAddShape").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceAddShape));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceRemoveBody").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceRemoveBody));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceRemoveShape").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceRemoveShape));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpacePointQuery").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpacePointQuery));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceSegmentQuery").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceSegmentQuery));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpSpaceAddCollisionHandler").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpSpaceAddCollisionHandler));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyNew").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyNew));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyNewStatic").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyNewStatic));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyFree").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyFree));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyGetVelocity").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyGetVelocity));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodySetVelocity").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodySetVelocity));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyGetPosition").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyGetPosition));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodySetPosition").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodySetPosition));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodySetForce").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodySetForce));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyGetForce").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyGetForce));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyGetMass").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyGetMass));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodySetMass").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodySetMass));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyGetRotation").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyGetRotation));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyApplyForceAtLocalPoint").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyApplyForceAtLocalPoint));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpBodyApplyImpulseAtLocalPoint").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpBodyApplyImpulseAtLocalPoint));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpCircleShapeNew").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpCircleShapeNew));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpPolyShapeNew").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpPolyShapeNew));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeGetBody").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeGetBody));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeFree").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeFree));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeSetCollisionType").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeSetCollisionType));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeGetCollisionType").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeGetCollisionType));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeSetFriction").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeSetFriction));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeSetElasticity").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeSetElasticity));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeSetFilter").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeSetFilter));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpShapeGetFilter").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpShapeGetFilter));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpMomentForCircle").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpMomentForCircle));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("cpMomentForBox").ToLocalChecked(),
      Nan::New<FunctionTemplate>(cpMomentForBox));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("createPathfindGrid").ToLocalChecked(),
      Nan::New<FunctionTemplate>(createPathfindGrid));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("resizePathfindGrid").ToLocalChecked(),
      Nan::New<FunctionTemplate>(resizePathfindGrid));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("pathfind").ToLocalChecked(),
      Nan::New<FunctionTemplate>(pathfind));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("raycast").ToLocalChecked(),
      Nan::New<FunctionTemplate>(raycast));
  functionTemplate->PrototypeTemplate()->Set(Nan::New("setPathfindNodeWeight").ToLocalChecked(),
      Nan::New<FunctionTemplate>(setPathfindNodeWeight));
  constructor.Reset(functionTemplate->GetFunction());
  constructorTemplate.Reset(functionTemplate);
}

void Kengi::New(const Nan::FunctionCallbackInfo<Value>& args) {
    Kengi* obj = new Kengi();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
}

Local<Object> Kengi::NewInstance() {
  Nan::EscapableHandleScope scope;
  Local<Function> cons = Nan::New<Function>(constructor);
  Local<Object> instance = cons->NewInstance();
  return scope.Escape(instance);
}

void Kengi::cpSpaceNew(const Nan::FunctionCallbackInfo<Value>& args) {
  Local<Object> instance = cpSpaceRef::NewInstance();
  std::shared_ptr<cpSpace>* spaceShared = new std::shared_ptr<cpSpace>(::cpSpaceNew(), &null_deleter);
  ::cpSpaceSetUserData(spaceShared->get(), spaceShared);
  cpSpaceRef* spaceRef = ObjectWrap::Unwrap<cpSpaceRef>(instance);
  spaceRef->space = *spaceShared;
  args.GetReturnValue().Set(instance);
}

void Kengi::cpSpaceFree(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(obj)){
      cpSpaceRef* spaceRef = ObjectWrap::Unwrap<cpSpaceRef>(obj);
      std::shared_ptr<cpSpace> spaceArg = spaceRef->space.lock();
      if(spaceArg) {
        if(spaceRef->handlerCount == 0){
          queryBodyCount = 0;
          ::cpSpaceEachBody(spaceArg.get(), Kengi::queryBody, 0);
          if(queryBodyCount == 0){
            queryShapeCount = 0;
            ::cpSpaceEachShape(spaceArg.get(), Kengi::queryShape, 0);
            if(queryShapeCount == 0){
              std::shared_ptr<cpSpace>* spaceOwner = (std::shared_ptr<cpSpace>*)cpSpaceGetUserData(spaceArg.get());
              if(spaceOwner){
                delete spaceOwner;
              } else {
                std::cout << "Invalid cpSpace reference (owner)" << std::endl;
              }
              ::cpSpaceFree(spaceArg.get());
            } else {
              std::cout << "Cannot free cpSpace while it has a shape" << std::endl;
            }
          } else {
            std::cout << "Cannot free cpSpace while it has a body" << std::endl;
          }
        } else {
          std::cout << "Cannot free cpSpace while it has handlers" << std::endl;
        }
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceFree" << std::endl;
  }
}

void Kengi::cpSpaceSetGravity(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> obj = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(obj)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(obj)->space.lock();
      if(spaceShared) {
        Local<Object> vect = args[1]->ToObject();
        Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
        Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
        if(xval->IsNumber() && yval->IsNumber()){
          cpVect vect = {xval->NumberValue(), yval->NumberValue()};
          ::cpSpaceSetGravity(spaceShared.get(), vect);
        } else {
          std::cout << "Invalid cpVect object" << std::endl;
        }
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceSetGravity" << std::endl;
  }
}

void Kengi::cpSpaceSetDamping(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> obj = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(obj)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(obj)->space.lock();
      if(spaceShared) {
        ::cpSpaceSetDamping(spaceShared.get(), args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceSetDamping" << std::endl;
  }
}

void Kengi::cpSpaceUseSpatialHash(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsNumber() && args[2]->IsNumber()){
    Local<Object> obj = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(obj)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(obj)->space.lock();
      if(spaceShared) {
        ::cpSpaceUseSpatialHash(spaceShared.get(), args[1]->NumberValue(), args[2]->Int32Value());
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceUseSpatialHash" << std::endl;
  }
}

void Kengi::cpSpaceStep(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> obj = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(obj)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(obj)->space.lock();
      if(spaceShared) {
        ::cpSpaceStep(spaceShared.get(), args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceStep" << std::endl;
  }
}

void Kengi::cpMomentForCircle(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 3 && args[0]->IsNumber() && args[1]->IsNumber() && args[2]->IsNumber() && args[3]->IsObject()){
    double m = args[0]->NumberValue();
    double r1 = args[1]->NumberValue();
    double r2 = args[2]->NumberValue();
    Local<Object> vect = args[3]->ToObject();
    Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
    Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
    if(xval->IsNumber() && yval->IsNumber()){
      cpVect offset = {xval->NumberValue(),yval->NumberValue()};
      args.GetReturnValue().Set(Nan::New(::cpMomentForCircle(m, r1, r2, offset)));
    } else {
      std::cout << "Invalid cpVect object" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpMomentForCircle" << std::endl;
  }
}

void Kengi::cpMomentForBox(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 2 && args[0]->IsNumber() && args[1]->IsNumber() && args[2]->IsNumber()){
    double m = args[0]->NumberValue();
    double w = args[1]->NumberValue();
    double h = args[2]->NumberValue();
    args.GetReturnValue().Set(Nan::New(::cpMomentForBox(m, w, h)));
  } else {
    std::cout << "Invalid arguments for cpMomentForBox" << std::endl;
  }
}

void Kengi::cpBodyNew(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsNumber() && args[1]->IsNumber()){
    cpBody* body = ::cpBodyNew(args[0]->NumberValue(), args[1]->NumberValue());
    Local<Object> instance = cpBodyRef::NewInstance();
    std::shared_ptr<cpBody>* bodyShared = new std::shared_ptr<cpBody>(body, &null_deleter);
    cpBodySetUserData(body, bodyShared);
    cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(instance);
    bodyRef->body = *bodyShared;
    args.GetReturnValue().Set(instance);
  } else {
    std::cout << "Invalid arguments for cpBodyNew" << std::endl;
  }
}

void Kengi::cpBodyNewStatic(const Nan::FunctionCallbackInfo<Value>& args) {
  cpBody* body = ::cpBodyNewStatic();
  std::shared_ptr<cpBody>* bodyShared = new std::shared_ptr<cpBody>(body, &null_deleter);
  cpBodySetUserData(body, bodyShared);
  Local<Object> instance = cpBodyRef::NewInstance();
  cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(instance);
  bodyRef->body = *bodyShared;
  args.GetReturnValue().Set(instance);
}

void Kengi::cpBodyFree(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpBodyRef::HasInstance(obj)){
      std::shared_ptr<cpBody> bodyArg = ObjectWrap::Unwrap<cpBodyRef>(obj)->body.lock();
      if(bodyArg) {
        if(bodyArg->shapeList == 0){
          cpSpace* space = cpBodyGetSpace(bodyArg.get());
          if(space == 0){
            std::shared_ptr<cpBody>* bodyOwner = (std::shared_ptr<cpBody>*)cpBodyGetUserData(bodyArg.get());
            if(bodyOwner){
              delete bodyOwner;
            } else {
              std::cout << "Invalid cpBody reference (owner)" << std::endl;
            }
            ::cpBodyFree(bodyArg.get());
          } else {
            std::cout << "Cannot free cpBody while it has a space" << std::endl;
          }
        } else {
          std::cout << "Cannot free cpBody while it has shapes" << std::endl;
        }
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyFree" << std::endl;
  }
}

void Kengi::cpBodyGetPosition(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 0 && args[0]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(body);
      std::shared_ptr<cpBody> bodyShared = bodyRef->body.lock();
      if(bodyShared) {
        cpVect cpv = ::cpBodyGetPosition(bodyShared.get());
        Local<Object> vect = Nan::New<Object>();
        vect->Set(Nan::New("x").ToLocalChecked(), Nan::New(cpv.x));
        vect->Set(Nan::New("y").ToLocalChecked(), Nan::New(cpv.y));
        args.GetReturnValue().Set(vect);
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyGetPosition" << std::endl;
  }
}

void Kengi::cpBodyGetForce(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 0 && args[0]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(body);
      std::shared_ptr<cpBody> bodyShared = bodyRef->body.lock();
      if(bodyShared) {
        cpVect cpv = ::cpBodyGetForce(bodyShared.get());
        Local<Object> vect = Nan::New<Object>();
        vect->Set(Nan::New("x").ToLocalChecked(), Nan::New(cpv.x));
        vect->Set(Nan::New("y").ToLocalChecked(), Nan::New(cpv.y));
        args.GetReturnValue().Set(vect);
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyGetForce" << std::endl;
  }
}

void Kengi::cpBodyGetVelocity(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 0 && args[0]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(body);
      std::shared_ptr<cpBody> bodyShared = bodyRef->body.lock();
      if(bodyShared) {
        cpVect cpv = ::cpBodyGetVelocity(bodyShared.get());
        Local<Object> vect = Nan::New<Object>();
        vect->Set(Nan::New("x").ToLocalChecked(), Nan::New(cpv.x));
        vect->Set(Nan::New("y").ToLocalChecked(), Nan::New(cpv.y));
        args.GetReturnValue().Set(vect);
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyGetVelocity" << std::endl;
  }
}

void Kengi::cpBodyGetMass(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 0 && args[0]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(body);
      std::shared_ptr<cpBody> bodyShared = bodyRef->body.lock();
      if(bodyShared) {
        args.GetReturnValue().Set(Nan::New(::cpBodyGetMass(bodyShared.get())));
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyGetMass" << std::endl;
  }
}

void Kengi::cpBodySetMass(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
      if(bodyShared) {
        ::cpBodySetMass(bodyShared.get(), args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodySetMass" << std::endl;
  }
}

void Kengi::cpBodyGetRotation(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 0 && args[0]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(body);
      std::shared_ptr<cpBody> bodyShared = bodyRef->body.lock();
      if(bodyShared) {
        cpVect cpv = ::cpBodyGetRotation(bodyShared.get());
        Local<Object> vect = Nan::New<Object>();
        vect->Set(Nan::New("x").ToLocalChecked(), Nan::New(cpv.x));
        vect->Set(Nan::New("y").ToLocalChecked(), Nan::New(cpv.y));
        args.GetReturnValue().Set(vect);
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyGetRotation" << std::endl;
  }
}

void Kengi::cpBodySetPosition(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      Local<Object> vect = args[1]->ToObject();
      Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
      Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
      if(xval->IsNumber() && yval->IsNumber()){
        std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
        if(bodyShared) {
          cpVect vect = {xval->NumberValue(), yval->NumberValue()};
          ::cpBodySetPosition(bodyShared.get(), vect);
        } else {
          std::cout << "Invalid cpBody reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpVect object" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodySetPosition" << std::endl;
  }
}

void Kengi::cpBodySetVelocity(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      Local<Object> vect = args[1]->ToObject();
      Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
      Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
      if(xval->IsNumber() && yval->IsNumber()){
        std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
        if(bodyShared) {
          cpVect vect = {xval->NumberValue(), yval->NumberValue()};
          ::cpBodySetVelocity(bodyShared.get(), vect);
        } else {
          std::cout << "Invalid cpBody reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpVect object" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodySetVelocity" << std::endl;
  }
}

void Kengi::cpBodySetForce(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      Local<Object> vect = args[1]->ToObject();
      Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
      Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
      if(xval->IsNumber() && yval->IsNumber()){
        std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
        if(bodyShared) {
          cpVect vect = {xval->NumberValue(), yval->NumberValue()};
          ::cpBodySetForce(bodyShared.get(), vect);
        } else {
          std::cout << "Invalid cpBody reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpVect object" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodySetForce" << std::endl;
  }
}

void Kengi::cpBodyApplyForceAtLocalPoint(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      Local<Object> impulse = args[1]->ToObject();
      Local<Value> impulsex = impulse->Get(Nan::New("x").ToLocalChecked());
      Local<Value> impulsey = impulse->Get(Nan::New("y").ToLocalChecked());
      if(impulsex->IsNumber() && impulsey->IsNumber()){
        Local<Object> pos = args[2]->ToObject();
        Local<Value> posx = pos->Get(Nan::New("x").ToLocalChecked());
        Local<Value> posy = pos->Get(Nan::New("y").ToLocalChecked());
        if(posx->IsNumber() && posy->IsNumber()){
          std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
          if(bodyShared) {
            cpVect cpimpulse = {impulsex->NumberValue(), impulsey->NumberValue()};
            cpVect cpposition = {posx->NumberValue(), posy->NumberValue()};
            ::cpBodyApplyForceAtLocalPoint(bodyShared.get(), cpimpulse, cpposition);
          } else {
            std::cout << "Invalid cpBody reference (expired)" << std::endl;
          }
        } else {
          std::cout << "Invalid cpVect object (position)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpVect object (impulse)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyApplyForceAtLocalPoint" << std::endl;
  }
}

void Kengi::cpBodyApplyImpulseAtLocalPoint(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      Local<Object> impulse = args[1]->ToObject();
      Local<Value> impulsex = impulse->Get(Nan::New("x").ToLocalChecked());
      Local<Value> impulsey = impulse->Get(Nan::New("y").ToLocalChecked());
      if(impulsex->IsNumber() && impulsey->IsNumber()){
        Local<Object> pos = args[2]->ToObject();
        Local<Value> posx = pos->Get(Nan::New("x").ToLocalChecked());
        Local<Value> posy = pos->Get(Nan::New("y").ToLocalChecked());
        if(posx->IsNumber() && posy->IsNumber()){
          std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
          if(bodyShared) {
            cpVect cpimpulse = {impulsex->NumberValue(), impulsey->NumberValue()};
            cpVect cpposition = {posx->NumberValue(), posy->NumberValue()};
            ::cpBodyApplyImpulseAtLocalPoint(bodyShared.get(), cpimpulse, cpposition);
          } else {
            std::cout << "Invalid cpBody reference (expired)" << std::endl;
          }
        } else {
        std::cout << "Invalid cpVect object (position)" << std::endl;
      }
      } else {
        std::cout << "Invalid cpVect object (impulse)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpBodyApplyImpulseAtLocalPoint" << std::endl;
  }
}

void Kengi::cpSpaceAddBody(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      Local<Object> body = args[1]->ToObject();
      if(cpBodyRef::HasInstance(body)){
        std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
        if(spaceShared){
          std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
          if(bodyShared) {
            ::cpSpaceAddBody(spaceShared.get(), bodyShared.get());
          } else {
            std::cout << "Invalid cpBody reference (expired)" << std::endl;
          }
        } else {
            std::cout << "Invalid cpSpace reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpBody reference" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceAddBody" << std::endl;
  }
}

void Kengi::cpSpaceAddShape(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      Local<Object> shape = args[1]->ToObject();
      if(cpShapeRef::HasInstance(shape)){
        std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
        if(spaceShared){
          std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
          if(shapeShared) {
            ::cpSpaceAddShape(spaceShared.get(), shapeShared.get());
          } else {
            std::cout << "Invalid cpShape reference (expired)" << std::endl;
          }
        } else {
            std::cout << "Invalid cpSpace reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceAddShape" << std::endl;
  }
}

void Kengi::cpCircleShapeNew(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsNumber() && args[2]->IsObject()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
      if(bodyShared) {
        Local<Object> vect = args[2]->ToObject();
        Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
        Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
        if(xval->IsNumber() && yval->IsNumber()){
          cpVect vect = {xval->NumberValue(), yval->NumberValue()};
          cpShape* shape = ::cpCircleShapeNew(bodyShared.get(), args[1]->NumberValue(), vect);
          Local<Object> instance = cpShapeRef::NewInstance();
          std::shared_ptr<cpShape>* shapeShared = new std::shared_ptr<cpShape>(shape, &null_deleter);
          cpShapeSetUserData(shape, shapeShared);
          cpShapeRef* shapeRef = ObjectWrap::Unwrap<cpShapeRef>(instance);
          shapeRef->shape = *shapeShared;
          args.GetReturnValue().Set(instance);
        } else {
          std::cout << "Invalid cpVect object" << std::endl; 
        }
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpCircleShapeNew" << std::endl;
  }
}

void Kengi::cpPolyShapeNew(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 4 && args[0]->IsObject() && args[1]->IsNumber() && args[2]->IsArray() && args[3]->IsObject() && args[4]->IsNumber()){
    Local<Object> body = args[0]->ToObject();
    if(cpBodyRef::HasInstance(body)){
      std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
      if(bodyShared) {
        Local<Array> pointArrayLocal = Local<Array>::Cast(args[2]);
        std::vector<cpVect> pointArray;
        for(int i = 0; i != pointArrayLocal->Length(); i++){
          if(pointArrayLocal->Get(i)->IsObject()){
            Local<Object> point = pointArrayLocal->Get(i)->ToObject();
            Local<Value> x = point->Get(Nan::New("x").ToLocalChecked());
            Local<Value> y = point->Get(Nan::New("y").ToLocalChecked());
            if(x->IsNumber() && y->IsNumber()){
              pointArray.push_back({x->NumberValue(), y->NumberValue()});
            } else {
              std::cout << "Skipping invalid cpVect" << std::endl;
            }
          } else {
            std::cout << "Skipping invalid cpVect" << std::endl;
          }
        }
        if(pointArray.size() > 2 && pointArray.size() == args[1]->NumberValue()){
          Local<Object> transformLocal = args[3]->ToObject();
          Local<Value> a = transformLocal->Get(Nan::New("a").ToLocalChecked());
          Local<Value> b = transformLocal->Get(Nan::New("b").ToLocalChecked());
          Local<Value> c = transformLocal->Get(Nan::New("c").ToLocalChecked());
          Local<Value> d = transformLocal->Get(Nan::New("d").ToLocalChecked());
          Local<Value> tx = transformLocal->Get(Nan::New("tx").ToLocalChecked());
          Local<Value> ty = transformLocal->Get(Nan::New("ty").ToLocalChecked());
          if(a->IsNumber() && b->IsNumber() && c->IsNumber() && d->IsNumber() && tx->IsNumber() && ty->IsNumber()){
            cpTransform transform = {a->NumberValue(), b->NumberValue(), c->NumberValue(), d->NumberValue(), tx->NumberValue(), ty->NumberValue()};
            cpShape* shape = ::cpPolyShapeNew(bodyShared.get(), pointArray.size(), &pointArray[0], transform, args[4]->NumberValue());
            Local<Object> instance = cpShapeRef::NewInstance();
            std::shared_ptr<cpShape>* shapeShared = new std::shared_ptr<cpShape>(shape, &null_deleter);
            cpShapeSetUserData(shape, shapeShared);
            cpShapeRef* shapeRef = ObjectWrap::Unwrap<cpShapeRef>(instance);
            shapeRef->shape = *shapeShared;
            args.GetReturnValue().Set(instance);
          } else {
            std::cout << "Invalid cpTransform object" << std::endl; 
          }
        } else {
          std::cout << "Invalid cpVect array" << std::endl;
        }
      } else {
        std::cout << "Invalid cpBody reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid cpBody reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpPolyShapeNew" << std::endl;
  }
}

void Kengi::cpShapeSetFriction(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> shape = args[0]->ToObject();
    if(cpShapeRef::HasInstance(shape)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
      if(shapeShared) {
        ::cpShapeSetFriction(shapeShared.get(), args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeSetFriction" << std::endl;
  }
}

void Kengi::cpShapeSetElasticity(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> shape = args[0]->ToObject();
    if(cpShapeRef::HasInstance(shape)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
      if(shapeShared) {
        ::cpShapeSetElasticity(shapeShared.get(), args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeSetElasticity" << std::endl;
  }
}

void Kengi::cpSpaceRemoveShape(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      Local<Object> shape = args[1]->ToObject();
      if(cpShapeRef::HasInstance(shape)){
        std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
        if(spaceShared){
          std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
          if(shapeShared) {
            ::cpSpaceRemoveShape(spaceShared.get(), shapeShared.get());
          } else {
            std::cout << "Invalid cpShape reference (expired)" << std::endl;
          }
        } else {
            std::cout << "Invalid cpSpace reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceRemoveShape" << std::endl;
  }
}

void Kengi::cpSpacePointQuery(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 3 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsNumber() && args[3]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
      if(spaceShared){
        Local<Object> vect = args[1]->ToObject();
        Local<Value> xval = vect->Get(Nan::New("x").ToLocalChecked());
        Local<Value> yval = vect->Get(Nan::New("y").ToLocalChecked());
        if(xval->IsNumber() && yval->IsNumber()){
          Local<Object> filter = args[3]->ToObject();
          Local<Value> filterGroup = filter->Get(Nan::New("group").ToLocalChecked());
          Local<Value> filterCategories = filter->Get(Nan::New("categories").ToLocalChecked());
          Local<Value> filterMask = filter->Get(Nan::New("mask").ToLocalChecked());
          if(filterGroup->IsNumber() && filterCategories->IsNumber() && filterMask->IsNumber()){
            cpVect pos = {xval->NumberValue(), yval->NumberValue()};
            cpShapeFilter cpf = {(unsigned)filterGroup->NumberValue(), (unsigned)filterCategories->NumberValue(), (unsigned)filterMask->NumberValue()};
            queryList.clear();
            ::cpSpacePointQuery(spaceShared.get(), pos, args[2]->NumberValue(), cpf, &queryFunc, 0);
            Local<Array> ret = Nan::New<Array>();
            unsigned retCount = 0;
            for(unsigned i = 0; i != queryList.size(); i++){
              cpShape* shape = queryList[i];
              std::shared_ptr<cpShape>* shapeOwner = (std::shared_ptr<cpShape>*)cpShapeGetUserData(shape);
              if(shapeOwner && *shapeOwner){
                Local<Object> shapeRefLocal = cpShapeRef::NewInstance();
                cpShapeRef* shapeRef = ObjectWrap::Unwrap<cpShapeRef>(shapeRefLocal);
                shapeRef->shape = *shapeOwner;
                Nan::Set(ret, retCount, shapeRefLocal);
                retCount++;
              } else {
                std::cout << "Invalid cpShape skipped during query" << std::endl;
              }
            }
            args.GetReturnValue().Set(ret);
          } else {
            std::cout << "Invalid cpShapeFilter object" << std::endl;
          }
        } else {
          std::cout << "Invalid cpVect object" << std::endl; 
        }
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpacePointQuery" << std::endl;
  }
}

void Kengi::cpSpaceSegmentQuery(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 4 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsObject() && args[3]->IsNumber() && args[4]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
      if(spaceShared){
        Local<Object> avect = args[1]->ToObject();
        Local<Value> axval = avect->Get(Nan::New("x").ToLocalChecked());
        Local<Value> ayval = avect->Get(Nan::New("y").ToLocalChecked());
        if(axval->IsNumber() && ayval->IsNumber()){
          Local<Object> bvect = args[2]->ToObject();
          Local<Value> bxval = bvect->Get(Nan::New("x").ToLocalChecked());
          Local<Value> byval = bvect->Get(Nan::New("y").ToLocalChecked());
          if(bxval->IsNumber() && byval->IsNumber()){
            Local<Object> filter = args[4]->ToObject();
            Local<Value> filterGroup = filter->Get(Nan::New("group").ToLocalChecked());
            Local<Value> filterCategories = filter->Get(Nan::New("categories").ToLocalChecked());
            Local<Value> filterMask = filter->Get(Nan::New("mask").ToLocalChecked());
            if(filterGroup->IsNumber() && filterCategories->IsNumber() && filterMask->IsNumber()){
              cpVect apos = {axval->NumberValue(), ayval->NumberValue()};
              cpVect bpos = {bxval->NumberValue(), byval->NumberValue()};
              cpShapeFilter cpf = {(unsigned)filterGroup->NumberValue(), (unsigned)filterCategories->NumberValue(), (unsigned)filterMask->NumberValue()};
              queryList.clear();
              ::cpSpaceSegmentQuery(spaceShared.get(), apos, bpos, args[3]->NumberValue(), cpf, &segmentQueryFunc, 0);
              Local<Array> ret = Nan::New<Array>();
              unsigned retCount = 0;
              for(unsigned i = 0; i != queryList.size(); i++){
                cpShape* shape = queryList[i];
                std::shared_ptr<cpShape>* shapeOwner = (std::shared_ptr<cpShape>*)cpShapeGetUserData(shape);
                if(shapeOwner && *shapeOwner){
                  Local<Object> shapeRefLocal = cpShapeRef::NewInstance();
                  cpShapeRef* shapeRef = ObjectWrap::Unwrap<cpShapeRef>(shapeRefLocal);
                  shapeRef->shape = *shapeOwner;
                  Nan::Set(ret, retCount, shapeRefLocal);
                  retCount++;
                } else {
                  std::cout << "Invalid cpShape skipped during query" << std::endl;
                }
              }
              args.GetReturnValue().Set(ret);
            } else {
              std::cout << "Invalid cpVect object" << std::endl; 
            }
          } else {
            std::cout << "Invalid cpShapeFilter object" << std::endl;
          }
        } else {
          std::cout << "Invalid cpVect object" << std::endl; 
        }
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceSegmentQuery" << std::endl;
  }
}

void Kengi::cpSpaceRemoveBody(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      Local<Object> body = args[1]->ToObject();
      if(cpBodyRef::HasInstance(body)){
        std::shared_ptr<cpSpace> spaceShared = ObjectWrap::Unwrap<cpSpaceRef>(space)->space.lock();
        if(spaceShared){
          std::shared_ptr<cpBody> bodyShared = ObjectWrap::Unwrap<cpBodyRef>(body)->body.lock();
          if(bodyShared) {
            ::cpSpaceRemoveBody(spaceShared.get(), bodyShared.get());
          } else {
            std::cout << "Invalid cpBody reference (expired)" << std::endl;
          }
        } else {
            std::cout << "Invalid cpSpace reference (expired)" << std::endl;
        }
      } else {
        std::cout << "Invalid cpBody reference" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceRemoveBody" << std::endl;
  }
}

void Kengi::cpShapeFree(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpShapeRef::HasInstance(obj)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(obj)->shape.lock();
      if(shapeShared) {
        cpSpace* space = cpShapeGetSpace(shapeShared.get());
        if(space == 0){
          std::shared_ptr<cpShape>* shapeOwner = (std::shared_ptr<cpShape>*)cpShapeGetUserData(shapeShared.get());
          if(shapeOwner){
            delete shapeOwner;
          } else {
            std::cout << "Invalid cpShape reference (owner)" << std::endl;
          }
          ::cpShapeFree(shapeShared.get());
        } else {
          std::cout << "Cannot free cpShape while it has a space" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeFree" << std::endl;
  }
}

void Kengi::cpShapeGetBody(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpShapeRef::HasInstance(obj)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(obj)->shape.lock();
      if(shapeShared) {
        cpBody* body = ::cpShapeGetBody(shapeShared.get());
        if(body){
          std::shared_ptr<cpBody>* bodyOwner = (std::shared_ptr<cpBody>*)cpBodyGetUserData(body);
          if(bodyOwner && *bodyOwner){
            Local<Object> instance = cpBodyRef::NewInstance();
            cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(instance);
            bodyRef->body = *bodyOwner;
            args.GetReturnValue().Set(instance);
          } else {
            std::cout << "The shape has an invalid body" << std::endl;
          }
        } else {
          std::cout << "The shape does not have a body" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeGetBody" << std::endl;
  }
}

void Kengi::cpShapeSetCollisionType(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsNumber()){
    Local<Object> shape = args[0]->ToObject();
    if(cpShapeRef::HasInstance(shape)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
      if(shapeShared) {
        ::cpShapeSetCollisionType(shapeShared.get(), (unsigned)args[1]->NumberValue());
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeSetCollisionType" << std::endl;
  }
}

void Kengi::cpShapeGetCollisionType(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpShapeRef::HasInstance(obj)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(obj)->shape.lock();
      if(shapeShared) {
        unsigned collisionType = ::cpShapeGetCollisionType(shapeShared.get());
        args.GetReturnValue().Set(Nan::New(collisionType));
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeGetCollisionType" << std::endl;
  }
}


unsigned char Kengi::collisionHandler(cpArbiter *arb, struct cpSpace *space, cpDataPointer data){
  CP_ARBITER_GET_SHAPES(arb, a, b);
  if(arb->handler->userData){
    Nan::Callback* nanCallback = (Nan::Callback*)arb->handler->userData;
    std::shared_ptr<cpShape>* aShared = (std::shared_ptr<cpShape>*)cpShapeGetUserData(a);
    std::shared_ptr<cpShape>* bShared = (std::shared_ptr<cpShape>*)cpShapeGetUserData(b);
    if(aShared && bShared){
      if(*aShared && *bShared){
        Local<Object> aInstance = cpShapeRef::NewInstance();
        Local<Object> bInstance = cpShapeRef::NewInstance();
        cpShapeRef* aRef = ObjectWrap::Unwrap<cpShapeRef>(aInstance);
        cpShapeRef* bRef = ObjectWrap::Unwrap<cpShapeRef>(bInstance);
        aRef->shape = *aShared;
        bRef->shape = *bShared;
        Local<Value> aValue = aInstance;
        Local<Value> bValue = bInstance;
        Local<Value> values[2] = {aInstance, bInstance};
        Local<Value> callbackValue = nanCallback->Call(2, values);
        if(callbackValue->IsBoolean()){
          return callbackValue->ToBoolean()->Value() == true ? 1 : 0;
        } else {
          std::cout << "Invalid collision handler return value" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference" << std::endl;
    }
  } else {
    std::cout << "Invalid collision handler" << std::endl;
  }
  return 1;
}

void Kengi::cpSpaceAddCollisionHandler(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 3 && args[0]->IsObject() && args[1]->IsNumber() && args[2]->IsNumber()){
    Local<Object> space = args[0]->ToObject();
    if(cpSpaceRef::HasInstance(space)){
      cpSpaceRef* spaceRef = ObjectWrap::Unwrap<cpSpaceRef>(space);
      std::shared_ptr<cpSpace> spaceShared = spaceRef->space.lock();
      if(spaceShared) {
        if(args[3]->IsFunction()){
          cpCollisionHandler *handler = ::cpSpaceAddCollisionHandler(spaceShared.get(), (unsigned)args[1]->NumberValue(), (unsigned)args[2]->NumberValue());
          if(handler->userData){
            delete (Nan::Callback*)handler->userData;
          } else {
            spaceRef->handlerCount++;
          }
          Nan::Callback* nanCallback = new Nan::Callback(Local<Function>::Cast(args[3]));
          handler->beginFunc = Kengi::collisionHandler;
          handler->userData = nanCallback;
        } else if(args[3]->IsNull()){
          cpCollisionHandler *handler = ::cpSpaceAddCollisionHandler(spaceShared.get(), (unsigned)args[1]->NumberValue(), (unsigned)args[2]->NumberValue());
          if(handler->userData){
            delete (Nan::Callback*)handler->userData;
            spaceRef->handlerCount--;
          }
          handler->beginFunc = 0;
          handler->userData = 0;
        } else {
          std::cout << "Invalid collision handler" << std::endl;
        }
      } else {
        std::cout << "Invalid cpSpace reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpSpace reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpSpaceAddCollisionHandler" << std::endl;
  }
}

void Kengi::cpShapeSetFilter(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> shape = args[0]->ToObject();
    if(cpShapeRef::HasInstance(shape)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(shape)->shape.lock();
      if(shapeShared){
        Local<Object> filter = args[1]->ToObject();
        Local<Value> filterGroup = filter->Get(Nan::New("group").ToLocalChecked());
        Local<Value> filterCategories = filter->Get(Nan::New("categories").ToLocalChecked());
        Local<Value> filterMask = filter->Get(Nan::New("mask").ToLocalChecked());
        if(filterGroup->IsNumber() && filterCategories->IsNumber() && filterMask->IsNumber()){
          cpShapeFilter shapeFilter = {(unsigned)filterGroup->NumberValue(), (unsigned)filterCategories->NumberValue(), (unsigned)filterMask->NumberValue()};
          ::cpShapeSetFilter(shapeShared.get(), shapeFilter);
        } else {
          std::cout << "Invalid cpShapeFilter object" << std::endl;
        }
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeSetFilter" << std::endl;
  }
}

void Kengi::cpShapeGetFilter(const Nan::FunctionCallbackInfo<Value>& args) {
  if(args.Length() != 0 && args[0]->IsObject()) {
    Local<Object> obj = args[0]->ToObject();
    if(cpShapeRef::HasInstance(obj)){
      std::shared_ptr<cpShape> shapeShared = ObjectWrap::Unwrap<cpShapeRef>(obj)->shape.lock();
      if(shapeShared) {
        cpShapeFilter shapeFilter = ::cpShapeGetFilter(shapeShared.get());
        Local<Object> shapeFilterObject = Nan::New<Object>();
        shapeFilterObject->Set(Nan::New("group").ToLocalChecked(), Nan::New((unsigned)shapeFilter.group));
        shapeFilterObject->Set(Nan::New("categories").ToLocalChecked(), Nan::New((unsigned)shapeFilter.categories));
        shapeFilterObject->Set(Nan::New("mask").ToLocalChecked(), Nan::New((unsigned)shapeFilter.mask));
        args.GetReturnValue().Set(shapeFilterObject);
      } else {
        std::cout << "Invalid cpShape reference (expired)" << std::endl;
      }
    } else {
      std::cout << "Invalid cpShape reference (type)" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for cpShapeGetFilter" << std::endl;
  }
}

void Kengi::queryFunc(cpShape *shape, cpVect point, cpFloat distance, cpVect gradient, void *data)
{
	queryList.push_back(shape);
}

void Kengi::queryBody(cpBody* body, void* data){
  queryBodyCount++;
}

void Kengi::queryShape(cpShape* shape, void* data){
  queryShapeCount++;
}

void Kengi::segmentQueryFunc(cpShape *shape, cpVect point, cpVect normal, cpFloat alpha, void *data)
{
	queryList.push_back(shape);
}

void Kengi::createPathfindGrid(const Nan::FunctionCallbackInfo<v8::Value>& args){
  Local<Object> instance = pathfindGridRef::NewInstance();
  std::shared_ptr<pathfindGrid> gridShared = std::make_shared<pathfindGrid>();
  pathfindGridRef* gridRef = ObjectWrap::Unwrap<pathfindGridRef>(instance);
  gridRef->grid = gridShared;
  args.GetReturnValue().Set(instance);
}

void Kengi::resizePathfindGrid(const Nan::FunctionCallbackInfo<v8::Value>& args){
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsNumber() && args[2]->IsNumber()){
    Local<Object> gridArg = args[0]->ToObject();
    if(pathfindGridRef::HasInstance(gridArg)){
      std::shared_ptr<pathfindGrid> gridShared = ObjectWrap::Unwrap<pathfindGridRef>(gridArg)->grid;
      if(gridShared){
        gridShared->resize(args[1]->NumberValue(), args[2]->NumberValue());
      } else {
        std::cout << "Invalid pathfindGrid reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid pathfindGrid reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for resize" << std::endl;
  }
}

void Kengi::pathfind(const Nan::FunctionCallbackInfo<v8::Value>& args){
  if(args.Length() > 4 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsObject() && args[3]->IsNumber() && args[4]->IsNumber()){
    Local<Object> gridArg = args[0]->ToObject();
    if(pathfindGridRef::HasInstance(gridArg)){
      std::shared_ptr<pathfindGrid> gridShared = ObjectWrap::Unwrap<pathfindGridRef>(gridArg)->grid;
      if(gridShared){
        Local<Object> aArg = args[1]->ToObject();
        Local<Value> aX = aArg->Get(Nan::New("x").ToLocalChecked());
        Local<Value> aY = aArg->Get(Nan::New("y").ToLocalChecked());
        if(aX->IsNumber() && aY->IsNumber()){
          Local<Object> bArg = args[2]->ToObject();
          Local<Value> bX = bArg->Get(Nan::New("x").ToLocalChecked());
          Local<Value> bY = bArg->Get(Nan::New("y").ToLocalChecked());
          if(bX->IsNumber() && bY->IsNumber()){
            vec2i a(aX->NumberValue(), aY->NumberValue());
            vec2i b(bX->NumberValue(), bY->NumberValue());
            std::vector<vec2i> path = gridShared->pathfind(a, b, args[3]->NumberValue(), args[4]->NumberValue());
            Local<Array> array = Nan::New<Array>();
            for(unsigned i = 0; i != path.size(); i++){
              Local<Object> node = Nan::New<Object>();
              node->Set(Nan::New("x").ToLocalChecked(), Nan::New(path[i].x));
              node->Set(Nan::New("y").ToLocalChecked(), Nan::New(path[i].y));
              Nan::Set(array, i, node);
            }
            args.GetReturnValue().Set(array);
          } else {
            std::cout << "Invalid cpVect object" << std::endl; 
          }
        } else {
          std::cout << "Invalid cpVect object" << std::endl; 
        }
      } else {
        std::cout << "Invalid pathfindGrid reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid pathfindGrid reference" << std::endl;
    }
  } else {
      std::cout << "Invalid arguments for pathfind" << std::endl;
  }
}

void Kengi::setPathfindNodeWeight(const Nan::FunctionCallbackInfo<v8::Value>& args){
  if(args.Length() > 2 && args[0]->IsObject() && args[1]->IsObject() && args[2]->IsNumber()){
    Local<Object> gridArg = args[0]->ToObject();
    if(pathfindGridRef::HasInstance(gridArg)){
      std::shared_ptr<pathfindGrid> gridShared = ObjectWrap::Unwrap<pathfindGridRef>(gridArg)->grid;
      if(gridShared){
        Local<Object> posArg = args[1]->ToObject();
        Local<Value> x = posArg->Get(Nan::New("x").ToLocalChecked());
        Local<Value> y = posArg->Get(Nan::New("y").ToLocalChecked());
        if(x->IsNumber() && y->IsNumber()){
          vec2i pos(x->NumberValue(), y->NumberValue());
          gridShared->setNodeWeight(pos, args[2]->NumberValue());
        } else {
          std::cout << "Invalid cpVect object" << std::endl;
        }
      } else {
        std::cout << "Invalid pathfindGrid reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid pathfindGrid reference" << std::endl;
    }
  } else {
    std::cout << "Invalid arguments for setPathfindNodeWeight" << std::endl;
  }
}

void Kengi::raycast(const Nan::FunctionCallbackInfo<v8::Value>& args){
  if(args.Length() > 1 && args[0]->IsObject() && args[1]->IsObject()){
    Local<Object> gridArg = args[0]->ToObject();
    if(pathfindGridRef::HasInstance(gridArg)){
      std::shared_ptr<pathfindGrid> gridShared = ObjectWrap::Unwrap<pathfindGridRef>(gridArg)->grid;
      if(gridShared){
        Local<Object> aArg = args[1]->ToObject();
        Local<Value> aX = aArg->Get(Nan::New("x").ToLocalChecked());
        Local<Value> aY = aArg->Get(Nan::New("y").ToLocalChecked());
        if(aX->IsNumber() && aY->IsNumber()){
          Local<Object> bArg = args[2]->ToObject();
          Local<Value> bX = bArg->Get(Nan::New("x").ToLocalChecked());
          Local<Value> bY = bArg->Get(Nan::New("y").ToLocalChecked());
          if(bX->IsNumber() && bY->IsNumber()){
            vec2i a(aX->NumberValue(), aY->NumberValue());
            vec2i b(bX->NumberValue(), bY->NumberValue());
            bool ret = gridShared->raycast(a, b);
            args.GetReturnValue().Set(Nan::New(ret));
          } else {
            std::cout << "Invalid cpVect object" << std::endl; 
          }
        } else {
          std::cout << "Invalid cpVect object" << std::endl; 
        }
      } else {
        std::cout << "Invalid pathfindGrid reference (expired)" << std::endl;
      }
    } else {
        std::cout << "Invalid pathfindGrid reference" << std::endl;
    }
  } else {
      std::cout << "Invalid arguments for raycast" << std::endl;
  }
}
