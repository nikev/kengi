#ifndef __PATHFIND_GRID_REF_H__
#define __PATHFIND_GRID_REF_H__

#include "nan.h"
#include <memory>
#include <iostream>
#include "pathfindGrid.h"

class pathfindGridRef : public Nan::ObjectWrap {
  pathfindGridRef();
  virtual ~pathfindGridRef();

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static Nan::Persistent<v8::Function> constructor;
  static Nan::Persistent<v8::FunctionTemplate> constructorTemplate;

 public:
  static void Init();
  static v8::Local<v8::Object> NewInstance();
  static bool HasInstance(const v8::Local<v8::Object>& obj);
  std::shared_ptr<pathfindGrid> grid;
};

#endif
