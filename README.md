
# Kengi

A Node.js package for strategy games.

## Features

- Native bindings to Chipmunk2D

- A* Pathfinding

## Usage

- [Install node-gyp](https://github.com/nodejs/node-gyp#installation)

- Run `npm install kengi`

- Run `npm test` from the `node_modules/kengi` folder to verify the installation 

## Documentation

See test.js for usage examples.  Check kengi.h for complete list of APIs available. Also see the [Chipmunk2D documentation](https://chipmunk-physics.net/documentation.php) for more information about Chipmunk2D.
