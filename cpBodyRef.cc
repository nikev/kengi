#include "cpBodyRef.h"

using namespace v8;

Nan::Persistent<Function> cpBodyRef::constructor;
Nan::Persistent<FunctionTemplate> cpBodyRef::constructorTemplate;

cpBodyRef::cpBodyRef() {
}

cpBodyRef::~cpBodyRef() {
}

void cpBodyRef::Init() {
  Nan::HandleScope scope;
  Local<FunctionTemplate> newTemplate = Nan::New<FunctionTemplate>(New);
  newTemplate->SetClassName((Nan::New("cpBodyRef").ToLocalChecked()));
  newTemplate->InstanceTemplate()->SetInternalFieldCount(1);
  newTemplate->PrototypeTemplate()->Set(Nan::New("getHashCode").ToLocalChecked(),Nan::New<FunctionTemplate>(GetHashCode));
  constructor.Reset(newTemplate->GetFunction());
  constructorTemplate.Reset(newTemplate);
}

void cpBodyRef::GetHashCode(const Nan::FunctionCallbackInfo<v8::Value>& args){
  Local<Object> instance = args.This();
  cpBodyRef* bodyRef = ObjectWrap::Unwrap<cpBodyRef>(instance);
  std::shared_ptr<cpBody> sharedBody = bodyRef->body.lock();
  if(sharedBody){
    args.GetReturnValue().Set(Nan::New((unsigned)(size_t)sharedBody.get()));
  }
}

void cpBodyRef::New(const Nan::FunctionCallbackInfo<Value>& args) {
  cpBodyRef* obj = new cpBodyRef();
  obj->Wrap(args.This());
  args.GetReturnValue().Set(args.This());
}

Local<Object> cpBodyRef::NewInstance() {
  Nan::EscapableHandleScope scope;
  Local<Function> cons = Nan::New<Function>(constructor);
  Local<Object> instance = cons->NewInstance();
  return scope.Escape(instance);
}

bool cpBodyRef::HasInstance(const Local<Object>& obj){
  return Nan::New(constructorTemplate)->HasInstance(obj);
}
