#include "pathfindGrid.h"

void pathfindGrid::resize(unsigned width, unsigned height){
      nodes.clear();
      std::vector<Node> row;
      row.resize(width);
      nodes.resize(height, row);
      for(unsigned y = 0; y != height; y ++){
          for(unsigned x = 0; x != width; x++){
              nodes[y][x].pos.x = x;
              nodes[y][x].pos.y = y;
          }
      }
      openSetLookup.clear();
      openSetLookup.resize(width*height, 0);
      openSet.clear();
      openSet.resize(width*height, 0);
      openSetCount = 0;
      closeSet.clear();
      closeSet.resize(width*height, 0);
}

unsigned pathfindGrid::getNodeId(const vec2i& pos){
    return pos.y * nodes[0].size() + pos.x;
}

bool pathfindGrid::outofbounds(const vec2i& pos){
	return !((unsigned)pos.y < nodes.size() && (unsigned)pos.x < nodes[pos.y].size());
}

void pathfindGrid::setNodeWeight(const vec2i& pos, unsigned weight){
    nodes[pos.y][pos.x].weight = weight;
}

std::vector<vec2i> pathfindGrid::pathfind(const vec2i &a, const vec2i &b, int hStrength, int cycleLimit)
{
    std::vector<vec2i> path;
    if (!nodes.empty() && !nodes[0].empty() && !outofbounds(a) && !outofbounds(b) && !(a.x == b.x && a.y == b.y))
    {
        Node *origin = &nodes[a.y][a.x];
        Node *dest = &nodes[b.y][b.x];
        if (origin->weight != 4294967295 && dest->weight != 4294967295)
        {
            std::fill(openSetLookup.begin(), openSetLookup.end(), 0);
            std::fill(closeSet.begin(), closeSet.end(), 0);
            openSetCount = 0;
            origin->fcost = 0;

            //--begin-- add origin to minheap
            openSetCount++;
            openSet[openSetCount] = origin;
            //--end--

            Node *i = 0;
            int iterationcount = 0;
            while (openSetCount != 0)
            {
                //get top tile from the open set min heap
                i = openSet[1];

                //if this is the destination then return the path
                if (i == dest || iterationcount == cycleLimit)
                {
                    while (i != origin)
                    {
                        path.push_back(i->pos);
                        i = i->parent;
                    }
                    return path;
                }

                //--begin-- remove the tile from open set and add it to the close set
                openSetLookup[getNodeId(i->pos)] = 0;
                openSet[1] = openSet[openSetCount];
                openSetCount--;

                int j = 1;
                int k;

                while (true)
                {
                    k = j;
                    if ((2 * j + 1) <= openSetCount)
                    {
                        if (openSet[k]->fcost >= openSet[2 * k]->fcost)
                            j = 2 * k;
                        if (openSet[j]->fcost >= openSet[2 * k + 1]->fcost)
                            j = 2 * k + 1;
                    }
                    else if ((2 * j) <= openSetCount)
                    {
                        if (openSet[k]->fcost >= openSet[2 * k]->fcost)
                            j = 2 * k;
                    }

                    if (j != k)
                    {
                        Node *tempNode = openSet[k];
                        openSet[k] = openSet[j];
                        openSet[j] = tempNode;
                    }
                    else
                    {
                        break;
                    }
                }

                closeSet[getNodeId(i->pos)] = 1;
                //--end--

                //evaluate neighbors
                Node *neighbor;
                int gridx, gridy;
                for (int n = 0; n != 8; n++)
                {
                    switch (n)
                    {
                    case 0:
                        gridx = i->pos.x + 1;
                        gridy = i->pos.y;
                        break;
                    case 1:
                        gridx = i->pos.x;
                        gridy = i->pos.y - 1;
                        break;
                    case 2:
                        gridx = i->pos.x - 1;
                        gridy = i->pos.y;
                        break;
                    case 3:
                        gridx = i->pos.x;
                        gridy = i->pos.y + 1;
                        break;
                    case 4:
                        gridx = i->pos.x + 1;
                        gridy = i->pos.y - 1;
                        break;
                    case 5:
                        gridx = i->pos.x - 1;
                        gridy = i->pos.y - 1;
                        break;
                    case 6:
                        gridx = i->pos.x - 1;
                        gridy = i->pos.y + 1;
                        break;
                    case 7:
                        gridx = i->pos.x + 1;
                        gridy = i->pos.y + 1;
                        break;
                    }

                    //check bounds
                    if (outofbounds(vec2i(gridx, gridy)))
                        continue;

                    neighbor = &nodes[gridy][gridx];

                    //if the tile is in close set or is  an obstruction then continue to the next neighbor
                    if (closeSet[getNodeId(neighbor->pos)] != 0 || neighbor->weight == 4294967295)
                        continue;

                    //corner cutting
                    switch (n)
                    {
                    case 4:
                        if ((nodes[neighbor->pos.y][neighbor->pos.x - 1]).weight == 4294967295 ||
                            (nodes[neighbor->pos.y + 1][neighbor->pos.x]).weight == 4294967295)
                            continue;
                        break;

                    case 5:
                        if ((nodes[neighbor->pos.y][neighbor->pos.x + 1]).weight == 4294967295 ||
                            (nodes[neighbor->pos.y + 1][neighbor->pos.x]).weight == 4294967295)
                            continue;
                        break;

                    case 6:
                        if ((nodes[neighbor->pos.y][neighbor->pos.x + 1]).weight == 4294967295 ||
                            (nodes[neighbor->pos.y - 1][neighbor->pos.x]).weight == 4294967295)
                            continue;
                        break;

                    case 7:
                        if ((nodes[neighbor->pos.y][neighbor->pos.x - 1]).weight == 4294967295 ||
                            (nodes[neighbor->pos.y - 1][neighbor->pos.x]).weight == 4294967295)
                            continue;
                        break;
                    }

                    //If the current neighbor is not in the open set, add it to the open set and assign its parent as i
                    if (openSetLookup[getNodeId(neighbor->pos)] == 0)
                    {
                        neighbor->parent = i;

                        //--begin-- calculate gscore
                        int gscore = 0;
                        Node *giterator = neighbor;
                        while (giterator != origin) //walk from i back to the origin node
                        {
                            if (giterator->pos.x != giterator->parent->pos.x && giterator->pos.y != giterator->parent->pos.y)
                                gscore += (14 + giterator->weight);
                            else
                                gscore += (10 + giterator->weight);

                            giterator = giterator->parent;
                        }
                        //--end

                        //calculate fcost by adding gcost and hcost
                        neighbor->fcost = gscore + (abs(neighbor->pos.x - dest->pos.x) * hStrength + abs(neighbor->pos.y - dest->pos.y) * hStrength);

                        //--begin-- add neighbor to open set
                        openSetLookup[getNodeId(neighbor->pos)] = 1;
                        openSetCount++;
                        openSet[openSetCount] = neighbor;

                        int t = openSetCount;
                        while (t != 1)
                        {
                            if (openSet[t]->fcost <= openSet[t / 2]->fcost)
                            {
                                Node *tempNode = openSet[t / 2];
                                openSet[t / 2] = openSet[t];
                                openSet[t] = tempNode;
                                t /= 2;
                            }
                            else
                                break;
                        }
                        //--end--
                    }
                }

                iterationcount++;
            }
        }
    }
    return path;
}

bool pathfindGrid::raycast(const vec2i &a, const vec2i &b){
	if(outofbounds(a)||outofbounds(b))
		return false;

	int x0 = a.x;
	int y0 = a.y;

	int x1 = b.x;
	int y1 = b.y;

	int dx = abs(x1-x0);
	int dy = abs(y1-y0);

	int sx;
	if(x0 < x1)
		sx = 1;
	else 
		sx = -1;

	int sy;
	if(y0 < y1)
		sy = 1;
	else 
		sy = -1;

	int err = dx-dy;

	while(true)
	{

        if(nodes[y0][x0].weight == 4294967295){
            return false;
        }

		if(x0 == x1 && y0 == y1)
			return true;

		int e2 = 2*err;
		if(e2 > -dy)
		{
			err = err - dy;
			x0 = x0 + sx;
		}

		if(e2 <  dx)
		{
			err = err + dx;
			y0 = y0 + sy ;
		}
	}

}