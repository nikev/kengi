#ifndef __CPSPACEREF_H__
#define __CPSPACEREF_H__

#include "nan.h"
#include <memory>
#include <set>
#include <iostream>
#include "chipmunk/chipmunk.h"

class cpSpaceRef : public Nan::ObjectWrap {
  cpSpaceRef();
  virtual ~cpSpaceRef();

  static void New(const Nan::FunctionCallbackInfo<v8::Value>& args);
  static Nan::Persistent<v8::Function> constructor;
  static Nan::Persistent<v8::FunctionTemplate> constructorTemplate;

 public:
  static void Init();
  static v8::Local<v8::Object> NewInstance();
  static bool HasInstance(const v8::Local<v8::Object>& obj);
  std::weak_ptr<cpSpace> space;
  int handlerCount;
};

#endif
